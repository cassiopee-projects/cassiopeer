#' Install the package
#'
#' Remove and recover the jalhyd/node_modules before and after installation
#'
tmp_path <- NULL
jalhyd_path <- file.path(pkgload::pkg_path(), "inst/jalhyd")
if (dir.exists(jalhyd_path)) {
  tmp_path <- tempfile()
  file.rename(jalhyd_path, tmp_path)
  dir.create(jalhyd_path)
  file.copy(file.path(tmp_path, "bundle.min.js"),
            jalhyd_path)
}
devtools::install(dependencies = FALSE)
if (!is.null(tmp_path)) {
  unlink(jalhyd_path, recursive = TRUE, force = TRUE)
  file.rename(tmp_path, jalhyd_path)
}
