% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/Cassiopee.R
\name{Cassiopee}
\alias{Cassiopee}
\title{Load the code of Cassiopée in a V8 instance}
\usage{
Cassiopee()
}
\value{
A V8 context including the code of Cassiopée
}
\description{
Load the code of Cassiopée in a V8 instance
}
\examples{
library(CassiopeeR)
sv8 <- Cassiopee()
}
