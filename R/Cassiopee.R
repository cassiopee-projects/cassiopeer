#' Load the code of Cassiopée in a V8 instance
#'
#' @return A V8 context including the code of Cassiopée
#' @export
#'
#' @examples
#' library(CassiopeeR)
#' sv8 <- Cassiopee()
Cassiopee <- function() {
  ct <- V8::v8()
  ct$source(system.file("jalhyd", "bundle.min.js", package = "CassiopeeR"))
  return(ct)
}
