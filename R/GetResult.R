#' Retrieve results of one variable in a nub
#'
#' @param sv8 V8 context including the Cassiopée's code (See [Cassiopee]), a session (See [LoadSession]), and a calculated nub (See [CalcNub])
#' @param uid [character] identifying the calculated nub
#' @param key [character] the Id of the requested result variable
#' @param element optional [integer] index of the requested element in the result variable vector (returns all elements by default)
#'
#' @return [numeric] vector of the requested result variable.
#' @export
GetResult <- function(sv8, uid, key = "", element = NULL) {
    if(key == "") {
        key <- "vCalc"
    } else {
        key <- paste0("values.", key)
    }
    s <- paste0("jalhyd.Session.getInstance().findNubByUid(\"", uid, "\").result.resultElements")
    if(is.null(element)) {
        nRes <- sv8$get(paste0(s, ".length"))
        element <- 0:(nRes-1)
    }
    sapply(element, function(x) {sv8$get(paste0(s, "[", x, "].", key))})
}
